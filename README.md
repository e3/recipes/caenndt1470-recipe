# caenndt1470 conda recipe

Home: "https://gitlab.esss.lu.se/icshwi/nss-instruments/caenNdt1470/caenndt1470"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS caenndt1470 module
